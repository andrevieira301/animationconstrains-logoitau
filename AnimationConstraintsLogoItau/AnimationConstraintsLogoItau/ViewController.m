//
//  ViewController.m
//  AnimationConstraintsLogoItau
//
//  Created by Andre Vieira on 21/07/15.
//  Copyright (c) 2015 Andre Vieira. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *widthLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLogo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingLogo;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(IBAction)upLogo:(id)sender{
    
    self.topLogo.constant = 10;
    self.leadingLogo.constant = 14;
    self.widthLogo.constant = -10;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(IBAction)downLogo:(id)sender{
    self.topLogo.constant = 20;
    self.leadingLogo.constant = 24;
    self.widthLogo.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
}

@end
